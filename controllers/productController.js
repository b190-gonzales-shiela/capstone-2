const Product = require("../models/Product.js");

// CREATION OF PRODUCTS

module.exports.createProduct = (data) => {
		let newProduct = new Product({
			name : data.name,
			description : data.description,
			price : data.price,
			stock: data.stock
		});
		return newProduct.save().then((product, error) => {
			if (error) {
				return false;
			} else {
				return "Product has been succesfully created.";
			};
		});
};



// retrieving all products

module.exports.getAllProduct = () =>{
	return Product.find({  }).then(result =>{
		return result;
	})
};

// retrieving all active product

module.exports.getAllActive = () =>{
	return Product.find({ isActive: true }).then(result =>{
		return result;
	})
};

// retrieving a specific product

module.exports.getProduct = (reqParams) =>{
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
};

// update a product

module.exports.updateProduct = ( reqParams, reqBody ) => {
	let updateProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stock: reqBody.stock
	}
	
	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((product, err)=>{
		if (err) {
			return false;
		}else{
			return "Product has been successfully updated";
		}
	})
};

// Archive a product

module.exports.archiveProduct = (reqParams, reqBody) => {
	let updateActiveField = {
		isActive : false
	};
	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error) => {
		if (error) {
			return false;
		} else {
			return "Product has been succesfully archived. ";
		}
	});
};

// delete a product- admin function only

module.exports.deleteProduct = (reqParams) =>{
	return Product.deleteOne(reqParams.productId).then(result => {
		return result;
	})
};
