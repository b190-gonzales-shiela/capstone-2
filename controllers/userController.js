const User = require("../models/User.js");

const Product= require("../models/Product.js");

const Order=require ("../models/Order.js")

const auth=require("../auth.js");

const bcrypt=require("bcrypt");

/*
	1. use mongoose method "find" to find duplicate emails
	2. use .then method to send a response based on the result of the "find" method
*/

module.exports.checkEmailExists = ( reqBody ) => {
	return User.find( { email: reqBody.email } ).then( result => {
		// if there is an existing duplicate email
		if (result.length > 0){
			return true;
		}else{
		// if the user email is not yet registered in our database
			return false;
		}
	} )
};

// New User Registration

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		fullName: reqBody.fullName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password,10)
	})
	return newUser.save().then((user, error) =>{
		if (User.find( { email: reqBody.email } )) {
			return "Try another email, this email exists";
		}else{
			return "Congratulations you have been registered! Start shopping! Note: All orders are for pick-up only.";
		}
	})
};



// user login


module.exports.loginUser = ( reqBody ) => {
	return User.findOne( { email: reqBody.email } ).then(result =>{
		
		if(result === null){
			return "Same email exists.";
		
		} else {
			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			} else { 
				return "Wrong password. Re- enter your password";
			}
		}
	} )
} ;

// make user an admin

module.exports.makeAdmin = (userId) => {
	return User.findById(userId).then((result, err) => {
		if(err){
			console.log(err);
			return err;
		}
		// Change the status of the returned document to "true"
		result.isAdmin = "true";

		// Saves the updated object in the MongoDB database
		return result.save().then((updatedAdmin, saveErr) => {
			if (saveErr) {
				console.log(saveErr);
				return saveErr;
			} else {
				return updatedAdmin;
			}
		})
	})
}



	