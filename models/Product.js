const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, "Product is name is required"]
	},
	description: {
		type: String,
		required: [true, "Product description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive:{
		type: Boolean,
		default: true
	},
	createdOn:{
		type: Date,
		// creates a new "date" that stores the current date and time of the course creation
		default: new Date()
	}
	});
			
	
	

module.exports = mongoose.model("Product", productSchema);