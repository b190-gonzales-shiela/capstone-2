const express = require("express");
const router = express.Router();
const userController=require("../controllers/userController.js");
const productController=require("../controllers/productController.js");
const Order=require("../models/Order.js");
const User=require("../models/User.js");
const Product= require("../models/Product.js");
const auth = require("../auth.js");


// route for user order/checkout


router.post("/", auth.verify, async (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === true) {
		res.send("User only function.");
	} else {
  const newOrder = new Order(req.body);

  try {
    const savedOrder = await newOrder.save();
    res.status(200).json(savedOrder);
  } catch (err) {
    res.status(500).json(err);
  }
}
});
// get all orders of customers- admin only

router.get("/", (req,res,next) =>{
	if(auth.decode(req.headers.authorization).isAdmin !== false) {
		res.send("Admin only function.");
	} else {
	Order.find()
	.exec()
	.then(docs =>{
		res.status(200).json(docs);
	})
	.catch(err => {
		res.status(500).json({
			error: err
		})
	})
	}
});


// get specific order using orderId- admin only

router.get("/:orderId", auth.verify, (req,res,next) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send("Admin only function.");
	} else {
	Order.find()
	.exec()
	.then(docs =>{
		res.status(200).json(docs);
	})
	.catch(err => {
		res.status(500).json({
			error: err
		})
	})
	}
});

// get specific order using orderId- user only

router.get("/:userId/:orderId", (req,res,next) =>{
	if(auth.decode(req.headers.authorization).isAdmin !== false) {
		res.send("User only function.");
	} else {
	Order.find()
	.exec()
	.then(docs =>{
		res.status(200).json(docs);
	})
	.catch(err => {
		res.status(500).json({
			error: err
		})
	})
	}
});

// get all order using orderId- user only

router.get("/:userId", (req,res,next) =>{
	if(auth.decode(req.headers.authorization).isAdmin !== true) {
		res.send("User only function.");
	} else {
	Order.find()
	.exec()
	.then(docs =>{
		res.status(200).json(docs);
	})
	.catch(err => {
		res.status(500).json({
			error: err
		})
	})
	}
});
		
module.exports = router;
