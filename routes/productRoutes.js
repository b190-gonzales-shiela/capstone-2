const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

// Route for creating a product

router.post("/createproduct", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send("Admin function only");
	} else {
		productController.createProduct(req.body).then(resultFromController => res.send(resultFromController));
	}
});


// route for getting all products

router.get('/allproduct', (req, res)=>{
	productController.getAllProduct().then(resultFromController => res.send(resultFromController));
});

// route for getting all ACTIVE products
router.get('/activeproduct', (req, res) =>{
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
} );

// route for retrieving a specific products
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// route for updating a product
router.put('/:productId', auth.verify, (req, res) =>{
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
} );

// route for archiving a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send("Admin only function.");
	} else {
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController));
	}
});

// route for deleting a product

router.delete("/:productId/delete", auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin === false) {
		res.send("Admin only function.");
	} else {
		productController.deleteProduct(req.params).then(resultFromController => res.send(resultFromController));
	}
});

module.exports = router;

