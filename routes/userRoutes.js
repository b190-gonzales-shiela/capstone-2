const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth= require("../auth.js");


router.post('/checkEmail', ( req, res ) =>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
} )

router.post("/register", ( req, res ) => {
	userController.registerUser( req.body ).then(resultFromController => res.send(resultFromController));
} );


// route for user authentication
router.post("/login", ( req, res ) => {
	userController.loginUser( req.body ).then( resultFromController => res.send( resultFromController ) );
} );

// admin getting details of user

router.get("/details", auth.verify, (req, res) => {
	// 
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// admin making another admin

router.put("/:id/admin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.makeAdmin(req.params.id).then(resultFromController => res.send(resultFromController));
});



module.exports = router;


